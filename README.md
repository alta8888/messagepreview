# MessagePreview for Thunderbird

MessagePreview adds a column to the Thunderbird message list/threadpane which provides a tooltip with the message text on hover.